﻿using tech_test_payment_api.Domain.DTO;

namespace tech_test_payment_api.Domain.Services
{
    public interface IVendaService
    {
        Task<VendaDTO> RegistarVenda(RegistrarVendaDTO venda);
        Task<VendaDTO> BuscarVenda(Guid id);
        Task AtualizarVenda(Guid id, AtualizarVendaDTO venda);
    }
}
