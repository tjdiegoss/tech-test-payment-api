﻿using FluentValidation;

namespace tech_test_payment_api.Domain.Models.Validations
{
    public class VendaValidation : AbstractValidator<Venda>
    {
        public VendaValidation()
        {
            RuleFor(v => v.Data).NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido");
            RuleFor(v => v.StatusId).NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido");

            RuleFor(v => v.Status).NotNull().WithMessage("O campo {PropertyName} não pode estar nulo");

            RuleFor(v => v.Vendedor).NotNull().WithMessage("O campo {PropertyName} não pode estar nulo");
            RuleFor(v => v.Vendedor.CPF).NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido");
            RuleFor(v => v.Vendedor.CPF).NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido");
            RuleFor(v => v.Vendedor.Nome).NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido");
            RuleFor(v => v.Vendedor.Email).NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido");

            RuleFor(v => v.Pedido).NotNull().WithMessage("O campo {PropertyName} não pode estar nulo");
            RuleFor(v => v.Pedido.Itens.Count()).GreaterThan(0).WithMessage("O pedido deve conter no mínimo 1 item");
        }
    }
}
