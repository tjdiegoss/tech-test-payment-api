﻿using tech_test_payment_api.Domain.Enums;

namespace tech_test_payment_api.Domain.Models
{
    public class StatusVenda
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        public IEnumerable<Venda> Vendas { get; set; }

        public bool StatusValidoAlteracao(int novoStatus)
        {
            if (this.Id == (int)StatusVendaEnum.AguardandoPagamento && 
                (novoStatus == (int)StatusVendaEnum.PagamentoAprovado || 
                 novoStatus == (int)StatusVendaEnum.Cancelada))
            {
                return true;
            }

            if (this.Id == (int)StatusVendaEnum.PagamentoAprovado &&
                (novoStatus == (int)StatusVendaEnum.EnviadoParaTransportadora ||
                 novoStatus == (int)StatusVendaEnum.Cancelada))
            {
                return true;
            }

            if (this.Id == (int)StatusVendaEnum.EnviadoParaTransportadora &&
                novoStatus == (int)StatusVendaEnum.Entregue)
            {
                return true;
            }

            return false;
        }
    }
}
