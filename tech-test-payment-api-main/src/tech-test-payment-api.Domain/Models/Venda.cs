﻿namespace tech_test_payment_api.Domain.Models
{
    public class Venda : Entidade
    {
        public DateTime Data { get; set; }
        public int StatusId { get; set; }
        public Guid VendedorId { get; set; }
        public Guid PedidoId { get; set; }

        public StatusVenda Status { get; set; }
        public Vendedor Vendedor { get; set; }
        public Pedido Pedido { get; set; }
    }
}
