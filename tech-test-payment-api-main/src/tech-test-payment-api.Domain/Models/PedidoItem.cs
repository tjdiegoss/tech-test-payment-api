﻿namespace tech_test_payment_api.Domain.Models
{
    public class PedidoItem : Entidade
    {
        public string Produto { get; set; }
        public int Quantidade { get; set; }

        public Guid PedidoId { get; set; }
        public Pedido Pedido { get; set; }
    }
}
