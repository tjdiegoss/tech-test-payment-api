﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Domain.DTO
{
    public class RegistrarTelefoneDTO
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [Range(11,99, ErrorMessage = "O campo {0} deve ser entre {1} e {2} dígitos")]
        public int DDD { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [Range(111111111, 999999999, ErrorMessage = "O campo {0} deve conter 9 dígitos")]
        public int Numero { get; set; }
    }
}
