﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Domain.DTO
{
    public class RegistrarVendedorDTO
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string CPF { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public RegistrarTelefoneDTO Telefone { get; set; }
    }
}