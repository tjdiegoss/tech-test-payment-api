﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Domain.DTO
{
    public class RegistrarPedidoDTO
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public List<RegistrarPedidoItemDTO> Itens { get; set; }
    }
}
