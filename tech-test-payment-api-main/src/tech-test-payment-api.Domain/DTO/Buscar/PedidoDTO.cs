﻿namespace tech_test_payment_api.Domain.DTO
{
    public class PedidoDTO
    {
        public Guid Id { get; set; }
        public List<PedidoItemDTO> Itens { get; set; }
    }
}
