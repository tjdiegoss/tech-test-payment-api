﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tech_test_payment_api.Domain.DTO
{
    public class StatusVendaDTO
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}
