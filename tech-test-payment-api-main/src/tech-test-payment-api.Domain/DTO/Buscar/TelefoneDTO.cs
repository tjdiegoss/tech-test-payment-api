﻿namespace tech_test_payment_api.Domain.DTO
{
    public class TelefoneDTO
    {
        public Guid Id { get; set; }
        public int DDD { get; set; }
        public int Numero { get; set; }
    }
}
