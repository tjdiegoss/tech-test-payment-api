﻿namespace tech_test_payment_api.Domain.DTO
{
    public class VendedorDTO
    {
        public Guid Id { get; set; }
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public TelefoneDTO Telefone { get; set; }
    }
}
