﻿using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Domain.Repositories
{
    public interface IVendaRepository : IRepository<Venda>
    {
        Task<Venda> ObterVendaDadosCompletos(Guid id);
        Task<StatusVenda> BuscarStatusVenda(int statusVendaId);
    }
}
