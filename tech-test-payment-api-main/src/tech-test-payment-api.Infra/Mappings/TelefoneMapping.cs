﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Infra.Mappings
{
    public class TelefoneMapping : IEntityTypeConfiguration<Telefone>
    {
        public void Configure(EntityTypeBuilder<Telefone> builder)
        {
            builder.HasKey(t => t.Id);

            builder.Property(t => t.DDD).IsRequired().HasMaxLength(3);
            builder.Property(t => t.Numero).IsRequired().HasMaxLength(9);
        }
    }
}
