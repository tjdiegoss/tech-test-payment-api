﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Infra.Mappings
{
    public class PedidoMapping : IEntityTypeConfiguration<Pedido>
    {
        public void Configure(EntityTypeBuilder<Pedido> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasMany(p => p.Itens)
                .WithOne(pi => pi.Pedido)
                .HasForeignKey(pi => pi.PedidoId);
        }
    }
}
