﻿using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Domain.Models;
using tech_test_payment_api.Domain.Repositories;
using tech_test_payment_api.Infra.Context;

namespace tech_test_payment_api.Infra.Repositories
{
    public class VendaRepository : Repository<Venda>, IVendaRepository
    {
        public VendaRepository(ApplicationDbContext db) : base(db)
        {
            if (!db.StatusVenda.Any())
            {
                var status = new List<StatusVenda>()
                {
                    new StatusVenda { Id = 1, Descricao = "Aguardando Pagamento" },
                    new StatusVenda { Id = 2, Descricao = "Pagamento Aprovado" },
                    new StatusVenda { Id = 3, Descricao = "Enviado Para Transportadora" },
                    new StatusVenda { Id = 4, Descricao = "Entregue" },
                    new StatusVenda { Id = 5, Descricao = "Cancelada" }
                };

                db.StatusVenda.AddRange(status);
                db.SaveChanges();
            }
        }

        public async Task<StatusVenda> BuscarStatusVenda(int statusVendaId)
        {
            return await Db.StatusVenda.FindAsync(statusVendaId);
        }

        public async Task<Venda> ObterVendaDadosCompletos(Guid id)
        {
            var venda = await Db.Vendas.AsNoTracking()
                .Include(v => v.Status)
                .Where(v => v.Id == id)
                .FirstOrDefaultAsync();

            venda.Vendedor = await Db.Vendedores.AsNoTracking()
                .Include(v => v.Telefone)
                .Where(v => v.Id == venda.VendedorId)
                .FirstOrDefaultAsync();

            venda.Pedido = await Db.Pedidos.AsNoTracking()
                .Include(p => p.Itens)
                .Where(p => p.Id == venda.PedidoId)
                .FirstOrDefaultAsync();

            return venda;
        }
    }
}
