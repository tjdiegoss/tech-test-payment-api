﻿using FluentValidation;
using FluentValidation.Results;
using tech_test_payment_api.Domain.Models;
using tech_test_payment_api.Domain.Notifications;

namespace tech_test_payment_api.Services
{
    public abstract class Service
    {
        private readonly INotificador _notificador;

        public Service(INotificador notificador)
        {
            _notificador = notificador;
        }

        protected void Notificar(string mensagem)
        {
            _notificador.Handle(new Notificacao(mensagem));
        }

        protected void Notificar(ValidationResult validationResult)
        {
            foreach (var error in validationResult.Errors) Notificar(error.ErrorMessage);
        }

        protected bool ExecutarValidacao<TV, TE>(TV validacao, TE entidade) where TV : AbstractValidator<TE> where TE : Entidade
        {
            var validator = validacao.Validate(entidade);

            if (validator.IsValid) return true;

            Notificar(validator);

            return false;
        }
    }
}
