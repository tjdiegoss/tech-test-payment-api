﻿using AutoMapper;
using tech_test_payment_api.Domain.DTO;
using tech_test_payment_api.Domain.Enums;
using tech_test_payment_api.Domain.Models;
using tech_test_payment_api.Domain.Models.Validations;
using tech_test_payment_api.Domain.Notifications;
using tech_test_payment_api.Domain.Repositories;
using tech_test_payment_api.Domain.Services;

namespace tech_test_payment_api.Services
{
    public class VendaService : Service, IVendaService
    {
        private readonly IVendaRepository _repository;
        private readonly IMapper _mapper;

        public VendaService(IVendaRepository repository,
                            INotificador notificador,
                            IMapper mapper) : base(notificador)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<VendaDTO> RegistarVenda(RegistrarVendaDTO vendaDTO)
        {
            var venda = _mapper.Map<Venda>(vendaDTO);
            venda.StatusId = (int)StatusVendaEnum.AguardandoPagamento;
            venda.Status = await _repository.BuscarStatusVenda((int)StatusVendaEnum.AguardandoPagamento);

            if (!ExecutarValidacao(new VendaValidation(), venda)) return null;

            if (!ValidarCamposVenda(venda)) return null;

            await _repository.Adicionar(venda);
            return _mapper.Map<VendaDTO>(venda);
        }

        public async Task<VendaDTO> BuscarVenda(Guid id)
        {
            return _mapper.Map<VendaDTO>(await _repository.ObterVendaDadosCompletos(id));
        }

        public async Task AtualizarVenda(Guid id, AtualizarVendaDTO vendaDTO)
        {
            var venda = await _repository.ObterVendaDadosCompletos(id);

            if (venda is null)
            {
                Notificar("Venda não existe");
                return;
            }

            if (!venda.Status.StatusValidoAlteracao(vendaDTO.StatusId))
            {
                Notificar("Status inválido");
                return;
            }

            venda.Status = await _repository.BuscarStatusVenda(vendaDTO.StatusId);

            if (!ExecutarValidacao(new VendaValidation(), venda)) return;

            await _repository.Atualizar(venda);
        }

        private bool ValidarCamposVenda(Venda venda)
        {
            bool valido = true;

            if (!venda.Vendedor.ValidarCPF())
            {
                Notificar("CPF do vendedor inválido");
                valido = false;
            }

            if (!venda.Vendedor.ValidarEmail())
            {
                Notificar("E-mail do vendedor invalido");
                valido = false;
            }

            if (!venda.Vendedor.Telefone.NumeroValido())
            {
                Notificar("Número de telefone inválido");
                valido = false;
            }

            return valido;
        }
    }
}
