﻿using AutoMapper;
using tech_test_payment_api.Domain.DTO;
using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Configuration
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<RegistrarVendaDTO, Venda>();
            CreateMap<RegistrarVendedorDTO, Vendedor>();
            CreateMap<RegistrarTelefoneDTO, Telefone>();
            CreateMap<RegistrarPedidoDTO, Pedido>();
            CreateMap<RegistrarPedidoItemDTO, PedidoItem>();

            CreateMap<Venda, VendaDTO>();
            CreateMap<StatusVenda, StatusVendaDTO>();
            CreateMap<Vendedor, VendedorDTO>();
            CreateMap<Telefone, TelefoneDTO>();
            CreateMap<Pedido, PedidoDTO>();
            CreateMap<PedidoItem, PedidoItemDTO>();

            CreateMap<AtualizarVendaDTO, Venda>();


        }
    }
}
