using tech_test_payment_api.Domain.Enums;
using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Tests.Models
{
    public class StatusVendaTests
    {
        [Theory]
        [InlineData((int)StatusVendaEnum.AguardandoPagamento, (int)StatusVendaEnum.EnviadoParaTransportadora)]
        [InlineData((int)StatusVendaEnum.AguardandoPagamento, (int)StatusVendaEnum.Entregue)]
        [InlineData((int)StatusVendaEnum.PagamentoAprovado, (int)StatusVendaEnum.AguardandoPagamento)]
        [InlineData((int)StatusVendaEnum.PagamentoAprovado, (int)StatusVendaEnum.Entregue)]
        [InlineData((int)StatusVendaEnum.EnviadoParaTransportadora, (int)StatusVendaEnum.AguardandoPagamento)]
        [InlineData((int)StatusVendaEnum.EnviadoParaTransportadora, (int)StatusVendaEnum.PagamentoAprovado)]
        [InlineData((int)StatusVendaEnum.EnviadoParaTransportadora, (int)StatusVendaEnum.Cancelada)]
        public void StatusValidoAlteracaoDeveRetornarFalsoQuandoPassadoStatusIncorreto(int statusAnterior, int novoStatus)
        {
            var statusVenda = new StatusVenda()
            {
                Id = statusAnterior
            };

            var retorno = statusVenda.StatusValidoAlteracao(novoStatus);

            Assert.False(retorno);
        }

        [Theory]
        [InlineData((int)StatusVendaEnum.AguardandoPagamento, (int)StatusVendaEnum.PagamentoAprovado)]
        [InlineData((int)StatusVendaEnum.AguardandoPagamento, (int)StatusVendaEnum.Cancelada)]
        [InlineData((int)StatusVendaEnum.PagamentoAprovado, (int)StatusVendaEnum.EnviadoParaTransportadora)]
        [InlineData((int)StatusVendaEnum.PagamentoAprovado, (int)StatusVendaEnum.Cancelada)]
        [InlineData((int)StatusVendaEnum.EnviadoParaTransportadora, (int)StatusVendaEnum.Entregue)]
        public void StatusValidoAlteracaoDeveRetornarVerdadeiroQuandoPassadoStatusCorreto(int statusAnterior, int novoStatus)
        {
            var statusVenda = new StatusVenda()
            {
                Id = statusAnterior
            };

            var retorno = statusVenda.StatusValidoAlteracao(novoStatus);

            Assert.True(retorno);
        }
    }
}